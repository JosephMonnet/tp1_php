<?php
  include_once('../config/pdo.php');
  require_once('../models/candidat.php');
  require_once('../userManagers/User_candidat_Manager.php');
  $donnee = [];
  $user = new User_candidat($donnee);
  $user->setNom($_POST["nom"]);
  $user->setPrenom($_POST["prenom"]);
  $user->setMail($_POST["email"]);
  $user->setMdp($_POST["mot_de_passe"]);
  $user_controller = new User_candidat_Manager($pdo);
  $user_controller->addUser($user);

  header("Location: ../index.php");
  exit();
?>