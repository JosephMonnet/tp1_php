<?php
session_start();
include_once('../config/pdo.php');

$reponse_entreprise = $pdo->query('SELECT * FROM entreprise');
$donnees_entreprise = $reponse_entreprise->fetchAll();
foreach ($donnees_entreprise as $donnee_entreprise){
       if ( empty($_POST["email"]) or empty($_POST["mot_de_passe"])){
            header('Location: ../index.php');
            exit();
        } elseif ( in_array($_POST["email"], $donnee_entreprise) and in_array($_POST["mot_de_passe"],$donnee_entreprise)){
            $_SESSION['isConnected'] = True;
            $_SESSION['email'] = $_POST["email"];
            header('Location: ../home.php');
            exit(); 
        } else {
            header('Location:../index.php');
        }
}
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>index</title>
  <!-- <link rel="stylesheet" href="style.css"> -->
</head>
<body>
</body>
</html>