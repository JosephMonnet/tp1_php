<?php
session_start();
include_once('../config/pdo.php');

$reponse_admin = $pdo->query('SELECT * FROM manager');
$donnees_admin = $reponse_admin->fetchAll();
foreach ($donnees_admin as $donnee_admin){
       if ( empty($_POST["email"]) or empty($_POST["mot_de_passe"])){
            header('Location: ../index.php');
            exit();
        } elseif ( in_array($_POST["email"], $donnee_admin) and in_array($_POST["mot_de_passe"],$donnee_admin)){
            $_SESSION['isConnected'] = True;
            $_SESSION['email'] = $_POST["email"];

            header('Location: ../home.php');
            exit(); 
        } else {
            header('Location: ../index.php');
        }
}
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>index</title>
  <!-- <link rel="stylesheet" href="style.css"> -->
</head>
<body>
<?php
    echo $_POST['email'];
    echo $_POST['mot_de_passe'];
?>
</body>
</html>