<?php
session_start();
include_once('../config/pdo.php');

$reponse_candidat = $pdo->query('SELECT * FROM candidat');
$donnees_candidat = $reponse_candidat->fetchAll();
foreach ($donnees_candidat as $donnee_candidat){
       if ( empty($_POST["email"]) or empty($_POST["mot_de_passe"])){
            header('Location: ../index.php');
            exit();
        } elseif ( in_array($_POST["email"], $donnee_candidat) and in_array($_POST["mot_de_passe"],$donnee_candidat)){
            $_SESSION['isConnected'] = True;
            $_SESSION['email'] = $_POST["email"];

            header('Location: ../home.php');
            exit(); 
        } else {
            header('Location: ../index.php');
        }
}
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>index</title>
  <!-- <link rel="stylesheet" href="style.css"> -->
</head>
<body>
<?php
    echo $_POST['email'];
    echo $_POST['mot_de_passe'];
?>
</body>
</html>