<?php
require_once('../models/entreprise.php');

class User_entreprise_Manager
{
    private $db; // Instance de PDO

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function setDb(PDO $db)
    {
        $this->db = $db;
    }

    public function getUser($id)
    {
        $id = (int) $id;

        $query = $this->db->prepare('SELECT * FROM entreprise WHERE id = :id');
        $query->bindParam(':id', $id);
        $query->execute();
        $donnees = $query->fetch(PDO::FETCH_ASSOC);

        if(!empty($donnees)){
        //    return new User($donnees);
        }else{
            return null;
        }

    }

    public function addUser(User_entreprise $user){
        $query = $this->db->prepare('INSERT INTO entreprise(id_manager, nom, mail, mdp) VALUES(1, :nom, :mail, :mdp)');

        $query->bindValue(':nom', $user->nom());
        $query->bindValue(':mail', $user->mail());
        $query->bindValue(':mdp', $user->mdp());

        $query->execute();
    }

    public function getAllUser()
    {
        $users = [];

        $query = $this->db->query('SELECT * FROM entreprise');
        $donnees = $query->fetchAll(PDO::FETCH_ASSOC);

        foreach ($donnees as $d){
         //   $users[] = new User($d);
        }

        return $users;
    }
}
?>