<?php
require_once('../models/candidat.php');

class User_candidat_Manager
{
    private $db; // Instance de PDO

    public function __construct($db)
    {
        $this->setDb($db);
    }

    public function setDb(PDO $db)
    {
        $this->db = $db;
    }

    public function getUser($id)
    {
        $id = (int) $id;

        $query = $this->db->prepare('SELECT * FROM candidat WHERE id = :id');
        $query->bindParam(':id', $id);
        $query->execute();
        $donnees = $query->fetch(PDO::FETCH_ASSOC);

        if(!empty($donnees)){
        //    return new User($donnees);
        }else{
            return null;
        }

    }

    public function addUser(User_candidat $user){
        $query = $this->db->prepare('INSERT INTO candidat(id_manager, nom, prenom, mail, mdp) VALUES(1, :nom, :prenom, :mail, :mdp)');

        $query->bindValue(':nom', $user->nom());
        $query->bindValue(':prenom', $user->prenom());
        $query->bindValue(':mail', $user->mail());
        $query->bindValue(':mdp', $user->mdp());

        $query->execute();
    }

    public function getAllUser()
    {
        $users = [];

        $query = $this->db->query('SELECT * FROM candidat');
        $donnees = $query->fetchAll(PDO::FETCH_ASSOC);

        foreach ($donnees as $d){
        //    $users[] = new User($d);
        }

        return $users;
    }
}

?>
